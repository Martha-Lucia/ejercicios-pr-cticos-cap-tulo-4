#Crear una funcion que pueda calcular la hipoteusa, a partir de sus catetos

#Autora= "Martha Cango"
#Email= "martha.cango@unl.edu.ec"


def calculo_hipotenusa(a,b):
    hipotenusa=(a**2)+(b**2)
    return hipotenusa


if __name__=="__main__":
    cateto1=float(input("Ingrese el primer cateto: "))
    cateto2=float(input("Ingrese el segundo cateto: "))
    print("la hipotenusa es: ",calculo_hipotenusa(cateto1, cateto2))



