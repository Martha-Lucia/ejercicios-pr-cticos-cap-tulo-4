#Programa para saludar con diferentes idiomas

#Autora= "Martha Cango"
#Email= "martha.cango@unl.edu.ec"


def greet(lang):
#Función para ayudar un idioma determinado
#Parámetro lang representa el idioma en el cual queremos representar
    if lang== "es":
        return "Hola"
    elif lang== "fr":
        return "Bonjour"
    else:
        return"Hello"


if __name__=="__main__":
    print("Inicios ...")
    print(greet("fr"), "Gaby")
    x=65
    print(greet("es"), "Stiven")
    x=x+2
