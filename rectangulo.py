#Programa para calcular el área de un rectángulo

#Autora = "Martha Cango"
#Email= "martha.cango@unl.edu.ec"

def calculo_area(base,altura):
    area=base*altura
    return area


if __name__=="__main__":
    base=float(input("Ingrese la base: "))
    altura=float(input("Ingrese la altura: "))
    area=calculo_area(base,altura)
    print("El area del rectángulo es: ", area)
