#Programa para calcular el área de un rectángulo

#Autora= "Martha Cango"
#Email= "martha.cango@unl.edu.ec"


def calculo_area(base,altura):
    area=base*altura
    return area


if __name__=="__main__":
    a=float(input("Ingrese la base: "))
    b=float(input("Ingrese la altura: "))
    area=calculo_area(b,a)
    #print("El area del rectangulo es: ", area)
    print("El area del rectángulo es: ",calculo_area(b,a))
