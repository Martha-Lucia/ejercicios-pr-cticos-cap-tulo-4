#Programa para calcular el área de un círculo

#Autora= "Martha Cango"
#Email= "martha.cango@unl.edu.ec"

import math


def calculo_area(radio):
    area=math.pi*radio**2
    return area


if __name__=="__main__":
    valor=float(input("Ingrese el valor de radio: "))
    print("El area del circulo es: ",calculo_area(valor))